package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	private int objectId;
	
	private String location;
	
	private double streetSegId;
	
	private double xCord;
	
	private double yCord;
	
	private String ticketType;
	
	private int fineamt;
	
	private int totalPaid;
	
	private int penalty1;
	
	private String accidentIndicator;
	
	private String ticketIssueDate;
	
	private String violationCode;
	
	private String violationDesc;
	
	public VOMovingViolations(int objectId, String location, double streetSegId, double xCord, double yCord,
			String ticketType, int fineamt, int totalPaid, int penalty1, String accidentIndicator,
			String ticketIssueDate, String violationCode, String violationDesc) {
		this.objectId = objectId;
		this.location = location;
		this.streetSegId = streetSegId;
		this.xCord = xCord;
		this.yCord = yCord;
		this.ticketType = ticketType;
		this.fineamt = fineamt;
		this.totalPaid = totalPaid;
		this.penalty1 = penalty1;
		this.accidentIndicator = accidentIndicator;
		this.ticketIssueDate = ticketIssueDate;
		this.violationCode = violationCode;
		this.violationDesc = violationDesc;
	}


	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDesc;
	}
}
