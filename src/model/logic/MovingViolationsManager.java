package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.DoubleLinkedList;
import model.data_structures.LinkedList;
import model.data_structures.NodoListaDoble;
import model.data_structures.NodoListaSencilla;

public class MovingViolationsManager implements IMovingViolationsManager {

	private DoubleLinkedList<String[]> vioList;
	
	public MovingViolationsManager()
	{
		vioList = new DoubleLinkedList<String[]>();
	}
	
	
	public void loadMovingViolations(String movingViolationsFile) throws IOException{
		// TODO Auto-generated method stub
		CSVReader csvReader = new CSVReaderBuilder(new BufferedReader( new FileReader(new File(movingViolationsFile)) )).withSkipLines(1).build();
		List<String[]> list = csvReader.readAll();
		
		for (String[] fila: list)
		{
			vioList.add(fila);
		}
		
	}

		
	@Override
	public DoubleLinkedList<VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		// TODO Auto-generated method stub
		DoubleLinkedList<VOMovingViolations> list = new DoubleLinkedList<VOMovingViolations>();
		
		for (int i = 0; i < vioList.size(); i++)
		{
			String[] current = vioList.get(i);
			
			if (current[11].equals(violationCode))
			{
				Integer oid = Integer.parseInt(current[0]);
				Double ssid = Double.parseDouble(current[2]);
				Double xc = Double.parseDouble(current[3]);
				Double yc = Double.parseDouble(current[4]);
				Integer fa = Integer.parseInt(current[6]);
				Integer tp = Integer.parseInt(current[7]);
				Integer pe = Integer.parseInt(current[8]);
				
				VOMovingViolations vomv = new VOMovingViolations(oid, current[1], ssid, xc, yc, current[5], fa, tp, pe, current[9], current[10], current[11], current[12]);
				list.add(vomv);
			}
		}
		
		return list;
	}

	@Override
	public DoubleLinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		// TODO Auto-generated method stub
		DoubleLinkedList<VOMovingViolations> list = new DoubleLinkedList<VOMovingViolations>();
		
		for (int i = 0; i < vioList.size(); i++)
		{
			String[] current = vioList.get(i);
			
			if (current[9].equals(accidentIndicator))
			{
				Integer oid = Integer.parseInt(current[0]);
				Double ssid = Double.parseDouble(current[2]);
				Double xc = Double.parseDouble(current[3]);
				Double yc = Double.parseDouble(current[4]);
				Integer fa = Integer.parseInt(current[6]);
				Integer tp = Integer.parseInt(current[7]);
				Integer pe = Integer.parseInt(current[8]);
				
				VOMovingViolations vomv = new VOMovingViolations(oid, current[1], ssid, xc, yc, current[5], fa, tp, pe, current[9], current[10], current[11], current[12]);
				list.add(vomv);
			}
		}
		
		return list;
	}	
	
	
}
