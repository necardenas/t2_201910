package api;

import java.io.FileNotFoundException;
import java.io.IOException;

import model.data_structures.DoubleLinkedList;
import model.data_structures.LinkedList;
import model.vo.VOMovingViolations;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IMovingViolationsManager {

	/**
	 * Method to load the Moving Violations of the STS
	 * @param movingViolationsFile - path to the file 
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 */
	void loadMovingViolations(String movingViolationsFile) throws FileNotFoundException, IOException;
	
	public DoubleLinkedList<VOMovingViolations> getMovingViolationsByViolationCode (String violationCode);
	
	
	public DoubleLinkedList<VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator);

	
}
